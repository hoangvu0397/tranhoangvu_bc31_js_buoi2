// BT1 
/**
 * Input
 * 
 * số ngày làm
 * lương 1 ngày: 100.000
 * Xử lí
 *  
 * Số ngày làm * lương 1 ngày
 * 
 * 
 * Output
 * 
 * Lương nhân viên
 */

 function salary (){

    var luong1Ngay = 100000
    var soNgayLam =     document.getElementById('soNgayLam').value *1;
    var luongValue = soNgayLam * luong1Ngay;
    luongValue = luongValue.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
        console.log("Salary: ", luongValue);
        document.getElementById('result1').innerHTML = `<div> Lương: ${luongValue} </div>`
    }
    // Input 
    // Nhập 5 số
    // Xử lí
    // Cộng 5 giá trị và chia cho 5
    // Output
    // Kết quả là trung bình cộng
    function nhapSo(){
        var nhapValue1 = document.getElementById('value1').value * 1;
        var nhapValue2 = document.getElementById('value2').value * 1;
        var nhapValue3 = document.getElementById('value3').value * 1;
        var nhapValue4 = document.getElementById('value4').value * 1;
        var nhapValue5 = document.getElementById('value5').value * 1;
        var tb = (nhapValue1+ nhapValue2+ nhapValue3 + nhapValue4 + nhapValue5)/5;
        console.log ("Giá trị: ", tb );
        document.getElementById('result2').innerHTML = `<div>Giá trị trung bình của 5 số thực trên là: ${tb}</div>`
    
    }
    // Input
    // Nhập tiền USD
    // Xử lí
    // Lấy giá trị USD vừa nhập nhân cho 23500
    // Output
    // Giá trị tiền Việt sau khi đổi
    function doiTien() {
        var usd = document.getElementById('USD').value*1;
        var doi = usd * 23500;
        doi = doi.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
    
        console.log("doi", doi)
        document.getElementById('result3').innerHTML =` <div>Giá trị tiền Việt sau khi đổi: ${doi}</div>`
    }
    // Input
    // Nhập chiều dài và chiều rộng 
    // Xử lí
    // Diện tích: Dài x Rộng
    // Chu vi: (Dài + Rộng) * 2
    // Output
    // Giá trị của chu vi và diện tích
    function tinhHcn(){
        var cr = document.getElementById('wid').value*1;
        var cd = document.getElementById('leng').value*1;
        var chuVi = (cr + cd)/2;
        var dienTich = cr * cd;
        document.getElementById('result4').innerHTML = `<div>Chu vi: ${chuVi} </div>`
        document.getElementById('result5').innerHTML = `<div>Diện Tích: ${dienTich} </div>`
    }
    // Input
    // Nhập số có 2 chữ số
    // Xử lí
    // Lấy số vừa nhập chia cho 10 và lấy phần dư
    // Lấy số vừa nhập chia cho 10 và lấy phần thương
    // Output
    // Số hàng đơn vị 
    // Số hàng chục
    function tong2KySo(){
        var nhapVa = document.getElementById('nhapValue').value*1;
        var  soHangDv = nhapVa % 10;
        var soHangChuc = nhapVa / 10;
        var tong  = soHangChuc + soHangDv;
        document.getElementById('result6').innerHTML = `<div>Tổng 2 ký số của số vừa nhập là: ${tong}</div>`;
    }
    // Em lỡ làm bài 1 có layout nên em lấy luôn code bài 1 để nộp cho bài 2 ạ